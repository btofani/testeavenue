package com.bruna.service;

public interface ProductService {
	
	public Product addProduct(Product product);
	
	public Product updateProduct(Product product);
	
	public Product deleteProduct(Product product);
	
	public List<Product> getAll();
}
