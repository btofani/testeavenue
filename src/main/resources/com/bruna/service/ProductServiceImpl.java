
public class ProductServiceImpl  implements ProductService{

	ProductDAO productDAO;
	
	public ProductService() {
		productDAO = new ProductDAO();
	}

	public Product addProduct(Product Product) {
		product = productDAO.addProduct();
		return product;
	}

	public Product updateProduct(Product Product) {
		product = productDAO.updateProduct();
		return product;
	}

	public void deleteProduct(Product id) {
		product = productDAO.deleteProduct();
		return product;
	}
	
	public List<Product> getAll(){
		List<Product> products = new ArrayList<Product>();
		products = productDAO.deleteProduct();
		return products;
	};
}
