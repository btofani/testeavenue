
@Path("/product")
public class ProductController {

	ProductService productService;
	
	@GET
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAll")
	public List<Product> getProducts() {
		List<Product> listProduct = productService.getAllProduct();
		return listProduct;
	}
	
	@PUT
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/addProduct")
	public Product addProduct(Product product) {
		Product product = productService.addProduct(product);
		return product;
	}
	
	@UPDATE
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/updateProduct")
	public Product updateProduct(Product product) {
		Product product = productService.updateProduct(product);
		return product;
	}
	
	@DELETE
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/deleteProduct")
	public Product deleteProduct(Product product) {
		Product product = productService.deleteProduct();
		return product;
	}
}
