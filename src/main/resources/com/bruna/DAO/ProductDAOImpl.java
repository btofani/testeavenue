
public interface ProductDAOImpl implements ProductDAO {

	private EntityManager em;
	
	public Product addProduct(Product product) {
		Product pro = new Product();
		pro = updateProduct(product);
	    return pro;
	}
	
	public Product updateProduct(Product product){
		EntityManager em = entityManagerFactory.createEntityManager();
		product = em.saveOrUpdate(product);
		return product;
	}
	
	public Product deleteProduct(Product product){
		EntityManager em = entityManagerFactory.createEntityManager();
		product = em.remove(product);
		return product;
	}
	
	public List<Product>getAll() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<Product> pro = em.createNamedQuery("Product.findAll", Product.class);
	    return pro;
	}
}
