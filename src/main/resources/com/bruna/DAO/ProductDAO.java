package com.bruna.DAO;

public class ProductDAO {
	
	public Product addProduct(Product product);
	
	public Product updateProduct(Product product);
	
	public Product deleteProduct(Product id);
	
	public List<Product> getAll();
	
}
