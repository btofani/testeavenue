
@Entity(name = "PRODUCT")
@NamedQueries({ 
	@NamedQuery(name = "Product.findAll", query = "SELECT pro FROM Product pro")
})
public class Product {

	@Id
	@Column(name = "PRODUCT_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "NAME")
	private String name;
	
	@Column(name = "IMG")
	private String img;
	
	Product product(Product product){
		return product;
	}
	
	public void setId(Long id) {
	    this.id = id;
	}
	public Long getId() {
	    return this.id;
	}
	
	public void setName(String id) {
	    this.name = name;
	}
	public String getName() {
	    return this.name;
	}
	
	public void setImg(String img) {
	    this.img = img;
	}
	public String getImg() {
	    return this.img;
	}
}
